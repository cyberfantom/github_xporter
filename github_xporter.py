#!/usr/bin/python
import os
import time

from prometheus_client import start_http_server, GC_COLLECTOR, PLATFORM_COLLECTOR, PROCESS_COLLECTOR
from prometheus_client.core import REGISTRY
from prometheus_client.twisted import MetricsResource
from twisted.internet import reactor
from twisted.web.resource import Resource
from twisted.web.server import Site

from collector import GithubCollector
from metrics import GitHubMetrics

# ENV vars. @TODO add cli params exposing
debug = int(os.environ.get('DEBUG', '0'))
port = int(os.environ.get('PORT', '9172'))
base_url = os.environ.get('BASE_URL', None)
token = os.environ.get('TOKEN', None)
http_server = os.environ.get('HTTP_SERVER', 'native')

# Set github metrics object
github_metrics = GitHubMetrics(token, base_url, debug)


def register_collector(collector):
    """ Register custom Prometheus collector in Registry. Unregister built-ins in prod mode"""
    if not debug:
        REGISTRY.unregister(GC_COLLECTOR)
        REGISTRY.unregister(PLATFORM_COLLECTOR)
        REGISTRY.unregister(PROCESS_COLLECTOR)
    REGISTRY.register(collector)


def run_http_server():
    """ Run http server """
    try:
        if http_server == 'native':
            start_http_server(port)
            print("Native http server. Serving at port: {}".format(port))
            while True:
                time.sleep(1)
        if http_server == 'twisted':
            root = Resource()
            root.putChild(b'metrics', MetricsResource())
            factory = Site(root)
            reactor.listenTCP(port, factory)
            print("Twisted http server. Serving at port: {}".format(port))
            reactor.run()
    except KeyboardInterrupt:
        print(" Interrupted")
        exit(0)


def main():
    """ Entry point """
    register_collector(GithubCollector(github_metrics))
    run_http_server()


if __name__ == "__main__":
    main()
