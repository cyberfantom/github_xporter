1. **Clone this repository**
2. **Build docker image:**
   ```bash
   docker build . -t github_xporter --force-rm
   ```
3. **Run docker container:**
   ```bash
   docker run -d \
       --name=github_xporter \
       -p 9172:9172 \
       -e BASE_URL="GITHUB_API_URL" \
       -e TOKEN="GITHUB_API_TOKEN" \
       github_xporter:latest
   ```
   Where GITHUB_API_URL custom API url if you're using it and GITHUB_API_TOKEN - your github access token for api calls.
   Also you can skip BASE_URL variable to use default github API url.
4. **Configure Prometheus**
   Add following job to prometheus yml config:
   ```yaml
   scrape_configs:
     ...
     - job_name: 'x_github'
       scrape_interval: 180s
       scrape_timeout: 180s
       static_configs:
         - targets: ['github_xporter:9172']
   ```
   Then, restart prometheus container.
5. **Import Grafana dashboard**
   Just import 'Github X Stats.json' dashboard file from this repository to Grafana as usual.
