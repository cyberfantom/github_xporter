from datetime import datetime

from prometheus_client.core import GaugeMetricFamily


class GithubCollector(object):
    """ Collector class for Prometheus metrics"""
    github_metrics = None
    _metrics = {}

    def __init__(self, github_metrics, debug=None):
        self.github_metrics = github_metrics
        self.debug = debug

    def init_metrics(self):
        """ Init empty Prometheus metrics """
        self._metrics = {}
        self._metrics = {
            'github_rate_limit': GaugeMetricFamily("github_rate_limit", 'Current Github requests Rate limit'),
            'github_rate_limit_remaining': GaugeMetricFamily("github_rate_limit_remaining",
                                                             'Current Github requests Rate limit remaining'),
            'github_rate_limit_reset': GaugeMetricFamily("github_rate_limit_reset",
                                                         'Current Github requests Rate limit reset time'),
            'github_code_commit_frequency': GaugeMetricFamily("github_code_commit_frequency", 'Code Commit Frequency',
                                                              labels=['repo']),
            'github_code_commit_frequency_w_total': GaugeMetricFamily("github_code_commit_frequency_w_total",
                                                                      'Code Commit Frequency weekly total',
                                                                      labels=['repo']),
            'github_code_commit_frequency_contributors_total': GaugeMetricFamily(
                "github_code_commit_frequency_contributors_total",
                'Code Commit Frequency by contributor total',
                labels=['repo', 'contributor']),
            'github_code_commit_frequency_contributors_w_total': GaugeMetricFamily(
                "github_code_commit_frequency_contributors_w_total",
                'Code Commit Frequency by contributor weekly total',
                labels=['repo', 'contributor']),
            'github_branch_age_days': GaugeMetricFamily("github_branch_age_days",
                                                        'Github Branch Age in Days',
                                                        labels=['repo', 'branch']),
            'github_branch_non_master_total': GaugeMetricFamily("github_branch_non_master_total",
                                                                'All non-master branches total',
                                                                labels=['repo']),
            'github_branch_master_size': GaugeMetricFamily("github_branch_master_size",
                                                           'Line of code in master branch',
                                                           labels=['repo']),
        }

    def set_metrics(self):
        """ Set metric. Called method must have the same name as metric """
        for metric_name in self._metrics:
            getattr(self, metric_name)()

    def collect(self):
        """ Collect method for Prometheus registry"""
        self.init_metrics()
        self.set_metrics()
        for metric in self._metrics.values():
            yield metric

    def github_rate_limit(self):
        """ Set 'Current Github requests Rate limit' metric """
        limit = self.github_metrics.rate.get('limit', 0)
        self._metrics['github_rate_limit'].add_metric([], limit)

    def github_rate_limit_remaining(self):
        """ Set 'Current Github requests Rate limit remaining' metric """
        limit_remaining = self.github_metrics.rate.get('limit_remaining', 0)
        self._metrics['github_rate_limit_remaining'].add_metric([], limit_remaining)

    def github_rate_limit_reset(self):
        """ Set 'Current Github requests Rate limit reset time' metric """
        limit_reset = self.github_metrics.rate.get('reset', 0)
        self._metrics['github_rate_limit_reset'].add_metric([], limit_reset)

    def github_code_commit_frequency(self):
        """ Set 'Code Commit Frequency' metric """
        commits = self.github_metrics.get_stats_commit_activity()
        for repo in commits:
            self._metrics['github_code_commit_frequency'].add_metric([repo], commits[repo]['today'])

    def github_code_commit_frequency_w_total(self):
        """ Set 'Code Commit Frequency weekly total' metric """
        commits = self.github_metrics.get_stats_commit_activity()
        for repo in commits:
            self._metrics['github_code_commit_frequency_w_total'].add_metric([repo], commits[repo]['weekly_total'])

    def github_code_commit_frequency_contributors_total(self):
        """ Set 'Code Commit Frequency by contributor total' metric """
        contributions = self.github_metrics.get_stats_contributors()
        for repo in contributions:
            for author in contributions[repo]:
                contributor = contributions[repo][author]
                total_commits = contributor.get('total_commits', 0)
                self._metrics['github_code_commit_frequency_contributors_total'].add_metric([repo, author],
                                                                                            total_commits)

    def github_code_commit_frequency_contributors_w_total(self):
        """ Set 'Code Commit Frequency by contributor weekly total' metric """
        contributions = self.github_metrics.get_stats_contributors()
        for repo in contributions:
            for author in contributions[repo]:
                week_data = contributions[repo][author]['week_data']
                weekly_commits = week_data.get('commits', 0)
                self._metrics['github_code_commit_frequency_contributors_w_total'].add_metric([repo, author],
                                                                                              weekly_commits)

    def github_branch_age_days(self):
        """ Set 'Github Branch Age in Days' metric """
        branches = self.github_metrics.get_branches()
        for repo in branches:
            for branch in branches[repo]:
                days_range = datetime.now() - branches[repo][branch]['date']
                self._metrics['github_branch_age_days'].add_metric([repo, branch], days_range.days)

    def github_branch_non_master_total(self):
        """ Set 'All non-master branches total' metric """
        branches = self.github_metrics.get_branches()
        for repo in branches:
            branches[repo].pop('master', None)
            self._metrics['github_branch_non_master_total'].add_metric([repo], len(branches[repo]))

    def github_branch_master_size(self):
        """ Set 'Line of code in master branch' metric """
        masters_size = self.github_metrics.get_master_size()
        for repo in masters_size:
            self._metrics['github_branch_master_size'].add_metric([repo], masters_size[repo])
