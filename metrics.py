import datetime
from pprint import pprint

from cachetools import cached, TTLCache
from github import Github, GithubException
from numba import jit

cache = {
    'repos': TTLCache(maxsize=10000, ttl=120),
    'rate': TTLCache(maxsize=10000, ttl=120),
    'stats_commit_activity': TTLCache(maxsize=10000, ttl=120),
    'stats_contributors': TTLCache(maxsize=10000, ttl=120),
    'branches': TTLCache(maxsize=10000, ttl=120),
    'master_size': TTLCache(maxsize=10000, ttl=120),
}


class GitHubMetrics:
    g = None

    def __init__(self, token, base_url=None, debug=None):
        self.debug = debug
        if base_url:
            self.g = Github(base_url=base_url, login_or_token=token)
        else:
            self.g = Github(token)

    @property
    @cached(cache['repos'])
    @jit(forceobj=True, nopython=True, parallel=True)
    def repos(self):
        """ fetch all repos """
        return self.g.get_user().get_repos()

    @property
    @cached(cache['rate'])
    def rate(self):
        """ Get current rate limit """
        try:
            rate_limit = self.g.get_rate_limit()
            result = {
                'limit': rate_limit.limit,
                'remaining': rate_limit.remaining,
                'reset': datetime.datetime.timestamp(rate_limit.reset)
            }

            return result
        except GithubException:
            return {}

    @cached(cache['stats_commit_activity'])
    @jit(forceobj=True, nopython=True, parallel=True)
    def get_stats_commit_activity(self):
        """ Get commit activity from last week """
        commits = {}
        today_index = int(datetime.datetime.today().strftime('%w'))
        for repo in self.repos:
            week_data = repo.get_stats_commit_activity()
            if week_data:
                latest_week = week_data[-1]
                commits.setdefault(repo.name, {})
                commits[repo.name].setdefault('days', latest_week.days)
                commits[repo.name].setdefault('weekly_total', latest_week.total)
                commits[repo.name].setdefault('today', latest_week.days[today_index])
                commits[repo.name].setdefault('timestamp', datetime.datetime.timestamp(latest_week.week))
        if self.debug:
            pprint(commits)

        return commits

    @cached(cache['stats_contributors'])
    @jit(forceobj=True, nopython=True, parallel=True)
    def get_stats_contributors(self):
        """ Get contributors commit activity total and from last week """
        contributors_stat = {}
        for repo in self.repos:
            contributors_stat.setdefault(repo.name, {})
            contributors = repo.get_stats_contributors()
            if contributors:
                for contributor in contributors:
                    week_data = {}
                    if contributor.weeks:
                        latest_week = contributor.weeks[-1]
                        week_data.setdefault('additions', latest_week.a)
                        week_data.setdefault('deletions', latest_week.d)
                        week_data.setdefault('commits', latest_week.c)

                    _data = {'week_data': week_data, 'total_commits': contributor.total}
                    contributors_stat[repo.name].setdefault(contributor.author.name, _data)

        return contributors_stat

    @cached(cache['branches'])
    @jit(forceobj=True, nopython=True, parallel=True)
    def get_branches(self):
        """ Get all branches data """
        branches = {}
        repo_map = {}
        for repo in self.repos:
            repo_map.setdefault(repo.name, {'obj': repo, 'branches': repo.get_branches()})
        for repo_name in repo_map:
            if repo_map[repo_name]['branches']:
                branches.setdefault(repo_name, {})
                for branch in repo_map[repo_name]['branches']:
                    branch_data = repo_map[repo_name]['obj'].get_branch(branch.name)
                    _data = {'author': getattr(branch_data.commit.commit.committer, 'name', None),
                             'date': getattr(branch_data.commit.commit.committer, 'date', None),
                             'sha': getattr(branch_data.commit.commit, 'sha', None)}
                    branches[repo_name].setdefault(branch.name, _data)

        return branches

    @cached(cache['master_size'])
    def get_master_size(self):
        """ Get all master branches sizes from all repos """
        size = {}
        for repo in self.repos:
            size.setdefault(repo.name, {})
            repo_contents = []
            total_size = 0
            try:
                repo_contents = repo.get_contents('/')
            except GithubException:
                pass
            if repo_contents:
                for file in repo_contents:
                    total_size += file.size
            size[repo.name] = total_size

        return size
