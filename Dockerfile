FROM python:3.6-slim

ENV PORT=9172

RUN mkdir -p /usr/src/app

ADD ./ /usr/src/app

WORKDIR /usr/src/app

RUN pip install --no-cache-dir wheel

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE ${PORT}

ENTRYPOINT [ "python", "-u", "./github_xporter.py" ]

# Build: docker build . -t github_xporter --force-rm
# Run: docker run github_xporter -e "TOKEN='your_github_token'"